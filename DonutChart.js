var data = {
    datasets: [{
        data: [
            55,
            45,
        ],
        backgroundColor: [
            "#FF6384",
            "#4BC0C0",
        ],
    }],
    labels: [
        "Leads nuevos",
        "Leads Trabajados",
    ]
};

var chartOptions = {
events: false,
  animation: {
    duration: 500,
    onComplete: function () {
      var ctx = this.chart.ctx;
      this.data.datasets.forEach(function (dataset) {
        for (var i = 0; i < dataset.data.length; i++) {
          var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model,
              total = dataset._meta[Object.keys(dataset._meta)[0]].total,
              mid_radius = model.innerRadius + (model.outerRadius - model.innerRadius)/2,
              start_angle = model.startAngle,
              end_angle = model.endAngle,
              mid_angle = start_angle + (end_angle - start_angle)/2;

          var x = mid_radius * Math.cos(mid_angle);
          var y = mid_radius * Math.sin(mid_angle);

          ctx.fillStyle = '#fff';
          if (i == 3){ // Darker text color for lighter background
            ctx.fillStyle = '#444';
          }
          var percent = String(Math.round(dataset.data[i]/total*100)) + "%";      
          //Don't Display If Legend is hide or value is 0
          if(dataset.data[i] != 0 && dataset._meta[1].data[i].hidden != true) {
            // Display percent in another line, line break doesn't work for fillText
            ctx.fillText(percent, model.x + x, model.y + y + 15);
          }
        }
      });               
    }
  }
};

var pieChartCanvas = $("#myDonutChart");
var pieChart = new Chart(pieChartCanvas, {
  type: 'doughnut',
  data: data,
  options: chartOptions
});