var ctx = document.getElementById('myChart').getContext('2d');

var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Chat', 'llamada', 'correo', 'facebook', 'twitter', 'instagram'],
        datasets: [{
            data: [11,14,39,8,9,16],
            backgroundColor: [
                '#39c2ca',
                '#278489',
                '#14474a',
                '#39c2ca',
                '#278489',
                '#14474a'
            ],
            borderWidth: 1,
            pointRadius: 0,
            fill: false,
        }]
    },
    options: {
        legend: {
            display: false,
        },
        showTooltips: false,
        animation: {
            onComplete: function() {
                var ctx = this.chart.ctx;
                ctx.textAlign = "center";
                ctx.textBaseline = "middle";
                var chart = this;
                var datasets = this.config.data.datasets;

                datasets.forEach(function(dataset, i) {
                    ctx.font = "15px arial";
                    ctx.fillStyle = "black";
                    chart.getDatasetMeta(i).data.forEach(function(p, j) {
                        ctx.fillText(datasets[i].data[j] + "%", p._model.x, p._model.y - 5);
                    });
                });
            }
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display:false
                }
            }],
            yAxes: [{
                gridLines: {
                    display:false,
                    drawBorder: false
                },
            }]
        },
        title: {
            display: false,
            text: 'Custom Chart Title'
        }
    }
});
